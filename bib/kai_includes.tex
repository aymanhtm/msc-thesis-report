%%	(c) 2014-2019 Benjamin Steinwender KAI GmbH
%%
%%	This file provides the style and recommended packages for
%%	a KAI software documentation LaTeX project.
%%

% encoding
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

%	fonts
\usepackage{lmodern}%	http://tex.stackexchange.com/questions/147194

\usepackage[
	usenames,
	dvipsnames,
	svgnames,
	table,
]{xcolor}
\usepackage[
	prefix=sol-,
]{xcolor-solarized}

\usepackage{booktabs}	%	\toprule, \midrule, \bottomrule
\usepackage{tabularx}	%	paragraph cells in table
\usepackage{ltablex}	%	tabularx over multiple pages
%\usepackage{multirow}
%\usepackage{rotating}

\usepackage{fmtcount}

\usepackage{titlecaps}	%	set sentence in uppercase (e.g. title)
\Addlcwords{is for an of}

\usepackage[bf,ruled]{caption}
\usepackage[pdftex]{graphicx}
\usepackage{wrapfig}
\usepackage{subcaption}
\renewcommand{\subfigureautorefname}{\figureautorefname}

\graphicspath{ {./pics/} }

\usepackage[
	version-1-compatibility,
	alsoload=binary,
	binary-units,
	per=slash,
]{siunitx}
\sisetup{
%	inter-unit-product = $\cdot$,
	list-final-separator = { \translate{and} },
	list-pair-separator = { \translate{and} },
	range-phrase = { \translate{to (numerical range)} },
	scientific-notation = engineering,	%	exponent is always a power of 3
	exponent-to-prefix,
	exponent-product = \cdot,
	retain-explicit-plus,
	detect-weight=true,
	detect-family=true,
	%	rounding
	round-mode = places,
	round-precision = 2,
	zero-decimal-to-integer,
	add-decimal-zero = false,
}
%	textcomp provides the textmu version that microtype can handle with siunitx
%		see http://tex.stackexchange.com/questions/74670
\usepackage{textcomp}
\usepackage{microtype}

\usepackage{nicefrac}

\usepackage{quotchap}	%	Decorative chapter headings
\usepackage{lettrine}	%	Drop first letter of paragraphs

\renewcommand{\raggeddictum}{\centering}

\usepackage{enumitem}
\usepackage{datetime}
\usepackage{qrcode}
\usepackage{dirtree}
\usepackage{appendix}

%\usepackage[
%	tikz,
%%	outputdir={graphs/},
%]{dot2texi}

\usepackage[
	lined,
	linesnumbered,
]{algorithm2e}

\usepackage[
	chapter,
	cache=false,
]{minted}				%	source code
\setminted{
	autogobble=true,
	linenos,
	tabsize=4,
	fontsize=\small,
}
\floatstyle{plaintop}\restylefloat{listing}	%	set the listing-caption above
\usepackage{scrhack}	%	fixes add float to lists - must be loaded after minted

\usepackage[ngerman,english]{babel}
\usepackage{csquotes}
\usepackage[	%	http://tex.stackexchange.com/questions/5091
	backend=biber,
%	backref=true,
	sorting=none,	%	sorting of citations: none = in order
	style=ieee,
	dashed=false,	%	in style ieee: do not print dashes for repeated authors
]{biblatex}
% https://tex.stackexchange.com/questions/451192
\usepackage{silence}
\WarningFilter{biblatex}{File 'english-ieee.lbx'}
\WarningFilter{biblatex}{File 'german-ieee.lbx'}
\WarningFilter{biblatex}{File 'ngerman-ieee.lbx'}

\usepackage{pgfplots}
\usepackage{pgfplotstable}
\usepgfplotslibrary{
	dateplot,
}
\pgfplotsset{compat=1.13}
\usepackage{tikz}
\usetikzlibrary{
	arrows,
	arrows.meta,
	automata,
	backgrounds,
	calc,
	decorations,
	decorations.markings,
	external,
	fit,
	pgfplots.groupplots,
	positioning,
	shadows,
	shapes,
	shapes.arrows,
	shapes.multipart,
}
%%	copy these lines to the main file
% \tikzexternalize[prefix=tikz/]
% \tikzset{external/optimize=false}

\usepackage[
	european,
	siunitx,
	RPvoltages,
]{circuitikz}


%	for warning box
\usepackage[tikz]{bclogo}
\newenvironment{info}%
{\begin{bclogo}[%
	logo=\bcinfo,%
	noborder=true,%
	couleurBarre=orange!70!yellow!100,%
	barre=snake,%
	tailleOndu=3.5,%
]{Information}}%
{\end{bclogo}}
\newenvironment{instruction}%
{\begin{bclogo}[%
	logo=\bcbook,%
	noborder=true,%
	couleurBarre=green!100,%
	barre=snake,%
	tailleOndu=3.5,%
]{Instructions}}%
{\end{bclogo}}
\newenvironment{warning}%
{\begin{bclogo}[%
	logo=\bcattention,%
	noborder=true,%
	couleurBarre=red!90,%
	barre=snake,%
	tailleOndu=3.5,%
]{Warning}}%
{\end{bclogo}}


%	macros
\newcommand{\eg}{e.g.\xspace}
\newcommand{\chnamref}[1]{\cref{#1} -- \nameref{#1}}

\newcommand{\mc}{microcontroller\xspace}
\newcommand{\mcs}{microcontrollers\xspace}
\newcommand{\Mc}{Microcontroller\xspace}
\newcommand{\Mcs}{Microcontrollers\xspace}
\newcommand{\MC}{Microcontroller\xspace}
\newcommand{\MCs}{Microcontrollers\xspace}

\newcommand{\state}[1]{\textbf{\textcolor{sol-yellow}{#1}}}
\newcommand{\event}[1]{\textit{\textcolor{sol-violet}{#1}}}

\newcommand\encircle[1]{%
	\tikz[baseline=-0.6ex]{\node[circle,red,draw,inner sep=1.5pt,scale=0.85] (0,0) {#1};}%
}


\usepackage[
	unicode=true,			%	non-Latin characters in Acrobat’s bookmarks
	hypertexnames=true,		%	fixes ``destination with the same identifier'' warning
	bookmarks=true,			%	show bookmarks bar?
	bookmarksnumbered=true,
	bookmarksopen=false,
%	breaklinks=true,		%	set true to word wrap long lines in listof* properly
	pdfborder={0 0 0},
%	pagebackref=true,		%	true|false	%	included in biblatex
	colorlinks=true,		%	true: colored links; false: boxes around the link
%	ocgcolorlinks=true,		%	Optional Content Groups - colored links, when viewed, but printed without colors: http://tex.stackexchange.com/questions/230752
	linkcolor=sol-blue,		%	color of internal links (change box color with linkbordercolor)
	citecolor=sol-green,	%	color of links to bibliography
	filecolor=sol-magenta,	%	color of file links
	urlcolor=sol-cyan,		%	color of external links
	pdfauthor={KAI GmbH},	%	author
]{hyperref}
\hypersetup{
%	pdftoolbar=true,		% show Acrobat’s toolbar?
%	pdfmenubar=true,		% show Acrobat’s menu?
	pdffitwindow=true,		% window fit to page when opened
	pdfstartview={FitH},	% fits the width of the page to the window
%	pdfsubject={Subject},	% subject of the document
%	pdfcreator={Creator},	% creator of the document
%	pdfproducer={Producer},	% producer of the document
%	pdfkeywords={keyword1, key2, key3}, % list of keywords
	pdfnewwindow=true,		% links in new PDF window
}

\usepackage{bookmark}

%	split hyperlink over line: http://tex.stackexchange.com/questions/47267
%	beware with OCG: the whole page will be colored in the linkcolor, when the link expands over a page
\makeatletter
\AtBeginDocument{%
	\newlength{\temp@x}%
	\newlength{\temp@y}%
	\newlength{\temp@w}%
	\newlength{\temp@h}%
	\def\my@coords#1#2#3#4{%
		\setlength{\temp@x}{#1}%
		\setlength{\temp@y}{#2}%
		\setlength{\temp@w}{#3}%
		\setlength{\temp@h}{#4}%
		\adjustlengths{}%
		\my@pdfliteral{\strip@pt\temp@x\space\strip@pt\temp@y\space\strip@pt\temp@w\space\strip@pt\temp@h\space re}}%
	\ifpdf
		\typeout{In PDF mode}%
		\def\my@pdfliteral#1{\pdfliteral page{#1}}% I don't know why % this command...
		\def\adjustlengths{}%
	\fi
	\def\Hy@colorlink#1{%
		\begingroup
			\ifHy@ocgcolorlinks
				\def\Hy@ocgcolor{#1}%
					\my@pdfliteral{q}%
					\my@pdfliteral{7 Tr}% Set text mode to clipping-only
			\else
				\HyColor@UseColor#1%
			\fi
		}%
	\def\Hy@endcolorlink{%
		\ifHy@ocgcolorlinks%
			\my@pdfliteral{/OC/OCPrint BDC}%
			\my@coords{0pt}{0pt}{\pdfpagewidth}{\pdfpageheight}%
			\my@pdfliteral{F}% Fill clipping path (the url's text) with current color
			\my@pdfliteral{EMC/OC/OCView BDC}%
			\begingroup%
				\expandafter\HyColor@UseColor\Hy@ocgcolor%
				\my@coords{0pt}{0pt}{\pdfpagewidth}{\pdfpageheight}%
				\my@pdfliteral{F}% Fill clipping path (the url's text) with \Hy@ocgcolor
			\endgroup%
			\my@pdfliteral{EMC}%
			\my@pdfliteral{0 Tr}% Reset text to normal mode
			\my@pdfliteral{Q}%
		\fi
		\endgroup
	}%
}
\makeatother

%	menukeys must be loaded after hyperref
\usepackage[
	os=win,
]{menukeys}

%	glossaries must be loaded after hyperref
\usepackage{mfirstuc}
\usepackage{mfirstuc-english}
\usepackage{makeidx}
\usepackage[
	toc,			%	include in table of contents
	nonumberlist,	%	suppress the location list
%	nogroupskip,	%	suppress alphabetic grouping of entries
	nopostdot,		%	suppress terminating dot
	acronym,		%	add acronyms
	notree,			%	no tree formats
	shortcuts,		%	shortcut commands (ac, acs, acl, acp, ...)
	savewrites,		%	reduce number of \write streams
]{glossaries}
\ifusesymbols
\newglossary[slg]{symbolslist}{syi}{syg}{Symbols}
\fi

%	non-breakable space between long and short form
\renewcommand*{\glsacspace}[1]{~}

%	use the description on first use,
\newacronymstyle{descfirst-long-sp-short}{%
	\GlsUseAcrEntryDispStyle{long-short}%
}{%
	\GlsUseAcrStyleDefs{long-short}%
	\renewcommand*{\genacrfullformat}[2]{%
		\glsentrydesc{##1}##2\glsacspace{##1}%
		(\protect\firstacronymfont{\glsentryshort{##1}})%
	}%
	\renewcommand*{\Genacrfullformat}[2]{%
		\Glsentrydesc{##1}##2\glsacspace{##1}%
		(\protect\firstacronymfont{\glsentryshort{##1}})%
	}%
	\renewcommand*{\genplacrfullformat}[2]{%
		\glsentrydescplural{##1}##2\glsacspace{##1}%
		(\protect\firstacronymfont{\glsentryshortpl{##1}})%
	}%
	\renewcommand*{\Genplacrfullformat}[2]{%
		\Glsentrydescplural{##1}##2\glsacspace{##1}%
		(\protect\firstacronymfont{\glsentryshortpl{##1}})%
	}%
}

\setacronymstyle{%
	descfirst-long-sp-short%custom format: see directly above
%	long-short%	on first use display the long form with the short form in parentheses
%	short-long%	on first use display the short form with the long form in parentheses
%	long-short-desc%	like long-short but you need to specify the description
%	short-long-desc%	like short-long but  you  need  to  specify  the  description
%	footnote%	on first use display the short form with a footnote on the page
%	dua%	always expand
}

%	cleveref must be loaded after hyperref
\usepackage[
	capitalize,
	nameinlink,
	noabbrev,
]{cleveref}

%	header style
\usepackage[
	automark,
	headsepline,
]{scrlayer-scrpage}
\clearpairofpagestyles
\cfoot[\pagemark]{\pagemark}
\lehead{\headmark}
\rohead{\headmark}
\pagestyle{scrheadings}

%	paragraph settings
\setlength{\parskip}{3ex plus 2ex minus 2ex}
\linespread{1.1}

\renewcommand{\floatpagefraction}{0.9}
\renewcommand{\textfraction}{0.05}
\renewcommand{\topfraction}{1.0}
\renewcommand{\bottomfraction}{1.0}

\usepackage[all]{nowidow}

\makeglossaries
\makeindex

%	don't print single occurences:	http://tex.stackexchange.com/questions/98494
\ifnosingle
\glsenableentrycount	%	enable \cgls, \cglspl, \cGls, \cGlspl
\let\gls\cgls
\let\glspl\cglspl
\let\Gls\cGls
\let\Glspl\cGlspl
\fi

%	load as very last package!
\usepackage[shortcuts]{extdash}

% prepare the acronyms and symbols list
\input{bib/abbr_symbols}
