#!/usr/bin/env sh
#
# This script will auto-build a LaTeX project.
#
# requirements in tex file:
#
#% arara: pdflatex: { interaction: nonstopmode, shell: true, synctex: true }
#% arara: biber: { options: [ '--isbn-normalise' ] }
#% arara: makeindex
#% arara: makeglossaries
#
#\usepackage{rerunfilecheck}

usage() {
	echo "This script will auto-build a LaTeX project." 1>&2
	echo "Usage: $0 [-c] [-v] [[-f] <file.tex>]" 1>&2
	exit 1
}

clean() {
	echo "clean everything"
	git clean -ffdx
}

# parse options: https://stackoverflow.com/questions/16483119
[ -e main.tex ] && main=main
while getopts ":cvf:" o; do
	case "${o}" in
		c)
			clean
			;;
		v)
			flags="--verbose "
			;;
		f)
			main=${OPTARG}
			;;
		*)
			usage
			;;
	esac
done
shift $((OPTIND-1))
if [ -z "${main}" ]; then
    main="$1"
fi
if [ -z "${main}" ]; then
    usage
fi

# repeat 'arara' until no more 'rerunfilecheck'
while : ; do
	arara $flags "${main}" || { grep -irnE "^!" "${main}.log"; exit 1; };	#	abort in case arara has failed
	warnings=$(grep -ic WARN "${main}.blg")
	if [ -z "$warnings" ]; then
		continue
	fi
	if (( $warnings > 0 )); then
		echo "Warnings during biber processing:"
		grep -irn WARN "${main}.blg"
		exit 1
	fi
	grep "rerun LaTeX" "${main}.log" && continue
	grep "Warning" "${main}.log" | grep -i rerun || break
done

g=$(git describe --always --dirty --abbrev)
d=$(git log --pretty=format:%ad -1 --date=short)
