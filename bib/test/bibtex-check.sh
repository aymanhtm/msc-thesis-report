#!/usr/bin/env sh

tmppath=tmp

mkdir -p $tmppath

fail=0
py=python3.6
$py --version 2> /dev/null
[[ $? -eq 0 ]] || py=python

i=library.bib
echo $i:
$py bibtex-check/bibtex_check.py -b $i || fail=1

exit $fail
