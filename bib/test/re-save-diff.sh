#!/usr/bin/env sh

tmppath=tmp
mkdir -p $tmppath

jr=lib/JabRef.jar
if [[ ! -e $jr ]]; then
	echo "JabRef not available ($jr)"
	exit -1
fi

curl https://artifactory.intra.infineon.com/artifactory/gen-kai-package-local/com/azul/zulu/zulu8.48.0.53-ca-fx-jdk8.0.265-win_x64.zip -O
unzip -f zulu8.48.0.53-ca-fx-jdk8.0.265-win_x64.zip -d lib/
export PATH=lib/zulu8.48.0.53-ca-fx-jdk8.0.265-win_x64/bin:$PATH

echo "Using Java: "
type java
java -version

echo "Using JabRef @ $jr"
java -jar $jr --version

fail=0

for i in $(ls lib*.bib); do
	exFile=$tmppath/$i.tmp
	rm -rf $exFile
	java -jar $jr -p jabref.prefs.xml -n -i $i,bibtex -o $exFile
	dos2unix $exFile
	diff -u $i $exFile || fail=1
done

exit $fail
