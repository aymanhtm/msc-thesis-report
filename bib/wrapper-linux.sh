#!/usr/bin/sh --login
echo original DISPLAY: ${DISPLAY}
export DISPLAY=$(hostname)${DISPLAY}
echo updated DISPLAY: ${DISPLAY}

module load kaisw/zulu/8 &> /dev/null

bsub -Is -R "select[ui=bib && osrel=70]" echo $DISPLAY; pwd; module list; timeout 45m ./run-tests.sh
