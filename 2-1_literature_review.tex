% ~x pages

\chapter{Literature Review}\label{ch:literature_review}

\section{JavaScript Object Notation}\label{sec:json}
\gls{JSON} is a lightweight data-interchange format.
It uses human-readable text to store and transmit data objects consisting of attribute–value pairs and array data types\footnote{JSON data interchange: \url{http://json.org}}.
JSON is becoming a clear choice for mainstream data applications \cite{Severance2012a}.
All the data files of the \gls{TDCSP} are represented in \gls{JSON} format, because of the last-mentioned advantages.
Therefore, a schema for each of the used files is written, following the JSON-schema draft \emph{Draft 2019-09}\footnote{Specification page: \url{http://json-schema.org}}.
\cref{lst:umops_config} shows an example of \gls{JSON} format representation.
Moreover, Python includes predefined modules which allows to generate and parse JSON-format data, this eases the coding developing.

\section{CAN Bus}\label{sec:can_bus}
\gls{CAN} is a multiplexed serial communication channel, used for data transfer among distributed electronic modules.
It emerged as the standard in-vehicle network. 
\gls{CAN} has several different physical layers.
These physical layers classify certain aspects of the \gls{CAN} protocol, such as electrical levels, signaling schemes, cable impedance, maximum baud rates, and more\footnote{White papers: Controller Area Network (CAN) Overview, \url{http://ni.com}}.
This section exhibits a brief explanation of the high-speed physical layer.

The distributed communication of the targets layer, in the \gls{MoPS} system, is of \gls{CAN} type.
Specifically, high-speed CAN is being used.
Nevertheless, for better maintainability of \gls{TDCSP}, the system model deals with the communication network in an abstract methodology, as described in \cref{ch:system_model}.

High-speed \gls{CAN} networks are implemented with two wires and allow communication at transfer rates up to \emph{1 Mbit/s}.
These two wires resemble two signals, \emph{CANL} and \emph{CANH}, either driven to a \emph{dominant} state with \emph{CANH (5V) > CANL (0V)}, or not driven and pulled by passive resistors to a \emph{recessive} state with \emph{CANH $\leq$ CANL}.
A $0$ data bit encodes a dominant state, while a $1$ data bit encodes a recessive state, supporting a wired-AND convention, which gives nodes with lower ID numbers priority on the bus.

Each node, of the network, requires: a \gls{CPU}, \gls{CAN} controller, and transceiver.
The latter converts the data stream from \gls{CAN} bus levels to levels that the \gls{CAN} controller uses.
Any node is able to send and receive messages, but not simultaneously.

A message or Frame consists primarily of the ID (identifier), which represents the priority of the message, and up to eight data bytes.
Other overhead are also part of the message.
Two different message formats can be used: using standard frames with 11bit identifiers (known as CAN 2.0 A) and extended frames with 29bit identifiers (known as CAN 2.0 B).
Except for the different length of the identifiers, the messages are built up equally.
The \gls{MoPS} system uses the standard frames.
\cref{fig:can_frame} illustrates the \gls{CAN} data frame sections.
\begin{figure}[htb]
    \centering
    \includegraphics[width=\linewidth]{can_data_frame}
    \caption{Standard \gls{CAN} data frame.}
    \label{fig:can_frame}
\end{figure}
\begin{description}
	\item[SOF] Start Of Frame - 1 bit.
	\item[IDENTIFIER] A message identifier sets the priority of the data frame - 11 bits.
	\item[RTR] Remote Transmission Request, defines the frame type (remote or data frame) - 1 bit.
	\item[IDE] Identifier Extension - 1 bit.
	\item[R] Reserved bit - 1 bit.
	\item[DLC] Data Length Code, number of bytes of data - 4 bits.
	\item[DATA FIELD] Data to be transmitted - 0-8 bytes.
	\item[CRC SEQUENCE] Cyclic Redundancy Check - 15 bits.
	\item[DEL] CRC delimiter - 1 bit.
	\item[ACK] Acknowledgement - 1 bit.
	\item[DEL] ACK delimiter - 1 bit.
	\item[EOF] End Of Frame - 7 bits.       
\end{description}

\section{MoPS}\label{sec:mops}
The \gls{MoPS} test system is a distributed test system.
The system architecture is shown in \cref{fig:mops_architecture}.
\gls{MoPS} design aims in providing a flexible infrastructure for customizable stress test applications.
It is capable of running different test applications with a common base framework.
The test engineers and test operators have to configure a simple comprehensive system, instead of having to manage different test systems \cite{Steinwender2013}.

To handle the complex requirements, \gls{MoPS} is split up into hierarchical entities.
The overall control entity, the local control entity, and the application entity.
\begin{description}
	\item[Overall Control Entity] Contains the host computer, which controls the overall test flow and communicates with the control modules. It also manages the external periphery and stores the measured data into the file system.
	\item[Local Control Entity] Contains control nodes, which may be many (typically 8 to 24 for one test system) and are connected to the host computer via Ethernet.
	\item[Application Entity] Contains application modules, which are referred as targets within the scope of this thesis. Each target is connected to one control module, from the local control entity. The target executes the test, drives and monitors a \gls{DUT}. All the targets share a \gls{CAN} communication. 
\end{description}
Both the control nodes and targets are typically placed within an environmental chamber. Only the host and the external periphery are placed outside.
The essential advantage of this test system architecture is the separation of the control and data acquisition parts from the actual test circuit.
Therefore, only the application entity has to be redesigned, when changing the type of test performed.
This saves development effort, design time and provides a unified data acquisition and control methodology.

\subsection{Targets Configuration}\label{ssec:targets_configuration}
The targets are tailored to individual types of tests.
Thus, every target is described within a configuration file, called \emph{Targets Configuration}.
\cref{lst:umops_config} shows a configuration of an example target.
The \emph{Targets Configuration} states the following:
\begin{itemize}
	\item Name of the target description.
	\item Both \gls{SW} and \gls{HW} versions of the target.
	\item List of the available tasks, which the target can execute.
\end{itemize}
Such files are described in \gls{JSON} format.
Therefore a schema respecting the description, is developed for checking the correctness of the file.
\begin{listing}[htp]
	\inputminted[frame=single]{JSON}{src/umops_config.json}
	\caption{µMoPS\_v5 configuration file snippet.}
	\label{lst:umops_config}
\end{listing}

\section{Scheduling Paradigms}\label{sec:scheduling_paradigms}
For a given set of tasks, the general scheduling problem can be understood as the problem of finding an order, according to which the tasks are to be executed such that various constraints are satisfied.
Typically, a task is characterized by its execution time, ready time, deadline, and resource requirements.
Tasks can have a variety of interpretations, depending on the field of study.
For the scope of this thesis, task resembles a computation that is executed by a \gls{CPU} in a sequential fashion, with an access to a communication network.

There exists an extensive literature on the topic of scheduling theory.
This section presents a small part of that extensive literature.
Only the methodologies, which are relevant to the research questions of this thesis, are discussed.
The scheduling problem variants can be summarized as follows:
\begin{description}
	\item[Machine Environment] Tasks can be scheduled using a \emph{mono-processor, multi-processor} or \emph{distributed} approach. The tasks of the \gls{MoPS} system are each linked with a single target and the targets share a distributed communication network, therefore, the scheduling problem contains both \emph{mono-processor} and \emph{distributed} approaches.
	\item[Release Time] In the static case, all the tasks are given and ready to run. In the dynamic case, new tasks may arrive at any time during the execution. The test plan, of the \gls{MoPS} system, is completely designed and planned, by the test engineers, before runtime. Therefore, the task set of the scheduling problem is considered static.
	\item[Execution Time] When dealing with real-time systems, execution times of tasks have to be known a priori, or a certain estimation has to be computed. For the scope of this thesis, \gls{WCET} is supposed as a given parameter, fetched from the \emph{Targets Configuration} files. Another project is emphasized in such execution time estimations, for the real-time extension of \gls{MoPS} \cite{Prabhakara2021a}.
	\item[Preemption] The execution time, of a task, might or might not be interrupted, thus, the terms preemptive and non-preemptive scheduling. For the \gls{MoPS} system, the tasks are not preemptive, therefore, the scheduling problem is non-preemptive.
	\item[Dependecies] Over the set of tasks, there might be precedence relation which constraints the order of execution. This is the case for the task set of the \gls{MoPS} system.
	\item[Periodicity] The tasks can be periodic, and therefore it is better described as a job. Otherwise, the tasks can be aperiodic. In the \gls{MoPS} system, the used tasks are considered aperiodic. 
	\item[Resources] Allocation of resources over time, to perform the task set. In the \gls{MoPS} system, the shared resources are: the \gls{CPU} between the tasks happening in the same target, and the communication network between the distinct targets.
	\item[Online/Offline] If release time is dynamic, then an online scheduling is required. Otherwise, if release time is static, then an offline scheduling can be done. In the \gls{MoPS} system, all the information of the test plan is known a priori, before runtime. Thus, the schedule is generated offline.
\end{description}

\subsection{Scheduling Problem}\label{ssec:scheduling_problem}
The key parameter of any scheduling problem is \emph{time}.
A task should be completed before its deadline, which in general is known a priori.
Moreover, over the set of tasks, there is precedence relation which constraints the order of execution.
Real-time scheduling refers to the case in which each task has its individual offset time (release time) and end time (deadline time) \cite{Liu1994a}.

In general, to form a scheduling problem, three sets are needed: a set of $n$ tasks $\Gamma = \{\tau_1,\tau_2,...,\tau_n\}$, a set of $m$ processors $P = \{p_1,p_2,...,p_n\}$, and a set of $k$ types of resources $R = \{r_1,r_2,...,r_n\}$.
In addition, precedence relations among the tasks can be defined through a \gls{DAG}, and the timing parameters are associated with each task.
Scheduling, thus, means assigning processors from $P$ and resources from $R$ to tasks from $\Gamma$, such that all tasks are completed respecting the imposed constraints \cite{Ecker1994a}.
This problem, in its general form, has been shown to be \emph{NP-complete}, and hence computationally intractable \cite{Buttazzo2011}.

\subsection{Scheduling Algorithms}\label{ssec:scheduling_algorithm}
The scheduling problem's variants are typically used to classify the various scheduling algorithms.
From those variants, the periodicity is the main classification, which encircles all the other variants.
Therefore, only the aperiodic scheduling algorithms are discussed.
In this context, the algorithm, fulfilling the requirements of this thesis, should consider:
\begin{itemize}
	\item Non-preemptive
	\item Static
	\item Precedence relations
	\item Offline
	\item Optimal
\end{itemize}
A description, which serves as a basis for the classification scheme, is used from \cite{Buttazzo2011}.
Such a notation uses three fields ($\alpha\;|\;\beta\;|\;\gamma$). 
Where $\alpha$ states the machine environment \emph{(mono-processor, multi-processor, distributed)}, $\beta$ describes tasks and resource characteristics \emph{(preemptive vs. non-preemptive, independent vs. precedence constraints,...)}, and $\gamma$ indicates the criterion of the objective function.
\begin{description}[style=nextline]
	\item[Earliest Due Date\label{itm:edd}]
		The scheme this algorithm considers is $1\;|\;SYNC\;|\;L_{max}$. That is, the set of tasks have a synchronous release time, and have to be scheduled on a single processor, minimizing the maximum latency. This algorithm was found by Jackson in 1955, which can be summarized as: execute the tasks in order of non-decreasing deadlines. \gls{EDD} is proven to be optimal with respect to minimizing the maximum latency \cite{Buttazzo2011}. The complexity of \gls{EDD} is $O(n\log n)$. No other constraints are considered, by \gls{EDD}, hence tasks cannot have precedence relations and cannot share resources.
	\item[Earliest Deadline First\label{itm:edf}]
		The problem this algorithm considers is $1\;|\;PREEM\;|\;L_{max}$. Tasks in this case, are not synchronous, and can have dynamic arrival times and preemption is allowed. \gls{EDF} was found by Horn in 1974, it can be summarized as: at any instant, execute the task with the earliest absolute deadline among the ready tasks. \gls{EDF} is proven to be optimal with respect to minimizing the maximum latency \cite{Buttazzo2011}. The complexity of \gls{EDF}, in this case per task, is $O(n\log n)$, if ready queue is a heap, or $O(n)$, if ready queue is a list. No other constraints are considered, by \gls{EDF} in this case, hence tasks cannot have precedence relations and cannot share resources.
	\item[Bratley's\label{itm:bratleys}]  
		This algorithm considered the scheme $1\;|\;NO\_PREEM\;|\;feasible$. Which is to say, a set of non-preemptive tasks with arbitrary arrivals times, have to be scheduled on a single processor. Bratley proposed this algorithm in 1971. Instead of the exhaustive search, Bratley’s uses a pruning technique to determine when a current search can be reasonably abandoned. The worst case complexity of Bratley’s is $O(n.n!)$. It can only be run off-line, which is the case for time-trigger systems but it is not optimal with respect to minimizing maximum latency.
	\item[Spring\label{itm:spring}]
		This algorithm aims in finding a feasible schedule when tasks have different types of constraints, such as precedence relations, resource constraints, arbitrary arrivals, non-preemptive properties, and importance levels. It was designed by Stankovic and Ramamritham and is used in distributed computer architectures. Therefore, Spring deals with \emph{NP-hard} problems \cite{Buttazzo2011}. Spring is not optimal, then if there is a feasible schedule, Spring may not find it.
	\item[Latest Deadline First\label{itm:ldf}]  
		The scheme this algorithm considers is $1\;|\;PREC,SYNC\;|\;L_{max}$. That is, minimizing maximum latency of a set of tasks, with precedence relations and synchronous arrivals, scheduled over a single processor. Lawler proposed \gls{LDF} in 1973. \gls{LDF} builds the scheduling queue from tail to head, by picking the latest deadline leaf of the \gls{DAG}, to be scheduled last. In this context, the first task inserted in the queue is executed last. The complexity of \gls{LDF} is $O(n^2)$, and it is proven optimal with respect to minimizing maximum latency \cite{Buttazzo2011}.
	\item[EDF Precedence Constraints\label{itm:edf_precedence_constraints}]  
		This algorithm considers the problem $1\;|\;PREC,PREEM\;|\;L_{max}$. Thus, scheduling a set of tasks with precedence relations and dynamic activations. Modified \gls{EDF} was proposed by Chetto, Silly, and Bouchentouf in 1990. The basic idea is to transform the set of dependant tasks into another set of independent tasks, by an adequate modifications of the timing parameters of each task \emph{(release times and deadlines)}. The transformation algorithm ensures optimality with respect to minimizing maximum latency \cite{Buttazzo2011}.
\end{description}

\subsubsection{Scheduling Time-Triggered Communication}\label{ssec:scheduling_time_triggered}
In time-triggered distributed systems, communication takes place according to a common periodic communication schedule \cite{Elmenreich2003a}. Therefore, discrete time slots are assigned for each node, where it is allowed to send and receive messages.
This provides an isolation between different functionalities and guarantees a deterministic latency, but makes the scheduling problem the bottleneck of the system.
The \gls{MoPS} system, containing a time-triggered distributed communication, compels a project-specific scheduling algorithm.
Such an algorithm is developed within this thesis, benefiting of the concepts offered by others algorithm, which do not completely consider the requirements. 

\section{Computational Complexity Theory}\label{sec:computational_complexity_theory}
In computational complexity theory, decision problems are classified based on the hardness of the problem.
This section gives a brief explanation of the fundamental complexity classes.
\begin{description}[style=nextline]
	\item[P Problems\label{itm:p_problem}]
		This kind of problems refer to all the decision problems that can be solved using a polynomial amount of computation time. Therefore, they are called \emph{Polynomial Time Problems}. P class problems are efficiently solvable by a deterministic Turing machine.
	\item[NP Problems\label{itm:np_problem}]
		This kind of problems cannot be solved in polynomial time. However, they can be verified in polynomial time. That is to say, NP class problems are solvable in polynomial time by a non-deterministic Turing machine \cite{Weisstein2020a}. P class problems are always also NP class.  
	\item[NP-Hard Problems\label{itm:np_hard_problem}] 
		A decision problem is said to be NP-hard, if and only if every problem in NP is reducible to it, in polynomial time.
	\item[NP-Complete Problems\label{itm:np_complete_problem}]
		NP-complete problems are the hardest between the NP problem class. A problem is classified as NP-complete, if it is both NP and NP-hard.
\end{description}
The real-time scheduling problem that this thesis is dealing with is of type NP-complete.
As a consequence, one of the research objectives is to identify a simpler and practical approach for finding a solution for such a problem.

\section{Time Utility Function}\label{sec:time_utility_function}
\gls{TUF} are created to quantify the utility of completing each task at a given time.
This concept aims in extending the pure deadline based real-time specification by a generic function.
The utility functions used in the scope of this thesis, are considered to be monotonically decreasing.
In this context, a \gls{TUF} returns a high value if the task is scheduled with minimum latency, and a low value if a task is scheduled with maximum latency.
\glspl{TUF} help in quantifying the time parameters assigned for every task, both the offset (release time) and end time (deadline time).
Therefore, \glspl{TUF} are essential in modeling the objective function of the scheduling problem of this thesis, which is fully described in \cref{ch:system_model}.

\section{Programming Languages}\label{sec:programming_languages}
When developing software, there are multiple factors that have to be considered for choosing the convenient programming language.
This section highlights some of those factors for different programming languages.
\begin{description}[style=nextline]
	\item[Java\label{itm:java}]
		Java is an object-oriented programming language that can be written on any device. It is platform independent in both binary and source level. Java is well known for its feature of safety which can disrupt corruptions or errors. Java requires high storage capacity and uses more memory, thus, it becomes slower in performance compared to other languages. 
	\item[Ruby\label{itm:ruby}]
		Ruby is a pure object-oriented programming language, everything appears to Ruby as an object. It is simple since it consists of easy and understandable syntax. Ruby on Rail is quite popular for web development. However, for developing desktop applications, Ruby is not so popular in the community. For big applications, Ruby is said to have a slow runtime speed.  
	\item[C/C++\label{itm:c_cpp}]
		C is one of the most difficult programming languages for software development. It is known to be the building block of many other languages used today. C++ is an object-oriented programming language, it offers the feature of platform independence. Also, since C++ is closely associated to C, then it allows low-level manipulation. However, C++ is still considered harder than other languages, it lacks the feature of garbage collector to automatically filter out unnecessary data.  
	\item[Python\label{itm:python}]
		Python is an interpreted programming language used for general-purpose programming. With a simple syntax, Python has automatic memory management and dynamic features that make it suitable to be used in a variety of applications. It offers a lot of third-party modules, which can ease in time the software development process. For Python everything is an object. Python is easy to read and learn, and it is one of the world's most popular programming languages. Therefore, there is a big community of Python developers, which is always of benefit for any software development.
\end{description}
Python language is chosen for developing the \gls{TDCSP}. The version of Python used is \emph{Python 3.8.2}.
\gls{TDCSP} is part of a bigger project, therefore future enhancements and integrations are probable.
The maintainability and the increasing community of Python, makes such integrations easier.

\subsection{Graphical User Interfaces}\label{ssec:guis}
Python offers multiple options for developing a \gls{GUI}.
This section exhibits the top \gls{GUI} tool-kits with a brief overview on each.
\begin{description}[style=nextline]
	\item[TKinter\label{itm:tkinter}]
		TKinter is an open source and standard \gls{GUI} toolkit for Python. TKinter is a wrapper around tcl/TK graphical interface. It is popular because of its simplicity and having an old active community. TKinter is portable for Macintosh, Windows and Linux platforms. However, it is mostly preferred for small scale \gls{GUI} applications.
	\item[PyQt\label{itm:pyqt}]
		PyQt toolkit is a wrapper around QT framework. PyQt is one of most popular cross platform Python binding over C++ implementing QT-library for QT framework. PyQT can be used for large scaled \gls{GUI} application. The design can be done with the use of QT designer and convert the \emph{.ui} files to Python code. However, writing code manually ensures better control of the code structure and representation.
	\item[PySide\label{itm:pyside}]
		Just like PyQt, PySide is also a Python binding of the cross-platform \gls{GUI} toolkit Qt. The two interfaces were comparable at first but PySide ultimately development lagged behind PyQt.   
\end{description}
For developing the \gls{GUI} of the \gls{TDCSP}, PyQt is chosen.
The version of PyQt used is \emph{PyQt5.15.0}.
