% ~25 pages

\chapter{Evaluations}\label{ch:evaluations}
This chapter covers the evaluation of the \emph{implementation} of the \gls{TDCSP}.
First, a complete \emph{test plan} is demonstrated in the \emph{Results} section.
In addition, a discussion is made on the efficiency of the test plan design along with the scheduling approach.
Finally, the proposed \emph{improvements} are presented, which are integrated into the \gls{TDCSP}.

\section{Results}\label{sec:results}
The problem statement of this thesis is divided into three areas, as described in \cref{sec:problem_statement}.
This section presents how the research questions are answered.
The answers are exhibited in coherence with the division of the problem statement.

\subsection{Test Plan Builder}
The implementation of the \emph{test plan} builder, resulted with an intuitive user interface.
\cref{fig:test_plan_builder} shows the \gls{GUI} of the builder.
\begin{figure}[htb]
    \centering
    \includegraphics[width=\linewidth]{example}
    \caption{Test plan builder.}
    \label{fig:test_plan_builder}
\end{figure}
On the left side, the targets window is depicted.
Within this window, the user is able to visualize all the instantiated targets, in the form of a hierarchical tree.
In this way, the user can easily understand the parent-child relation between the \emph{tasks} and the \emph{targets}.

On the left side, there is a wide scene area, for designing the precedence relations, required for the \emph{test plan}.
Therefore, the user can easily drag and drop the tasks over the scene, and define the \gls{DAG} by simple clicks between the ports of the tasks.
The test plan design, can become very complex, thus, the more space, the better it is for the user.
The builder scene can be zoomed in/out, for easing the design process.
Moreover, the builder window can be made the only window of the \gls{TDCSP}, i.e., all the other windows can be hidden, as shown in \cref{fig:extended_builder_scene}.
Hence, more space is offered for drawing the \gls{DAG} of the test plan.
\begin{figure}[htb]
    \centering
    \includegraphics[width=\linewidth]{example_extended}
    \caption{Extended builder scene.}
    \label{fig:extended_builder_scene}
\end{figure}

On the bottom side, the prompt window is depicted.
Within this window the user can see the log messages, while progressing with the design procedure.

\subsection{Test Plan Schedule}
The scheduling approach elaborated within this thesis, provided a convenient model for the real-time \emph{test plan}.
The optimal schedule time-line is achievable within negligible execution time, for the simple \emph{test plan} designs.
\cref{fig:test_plan_schedule} demonstrates the output schedule time-line, for the \emph{test plan} shown in \cref{fig:test_plan_builder}.
The displayed schedule time-line is optimal with respect to minimizing maximum latency.
Notice that the resolution is narrowed for showing the complete time-line at once without the need to scroll.
\begin{figure}[htb]
    \centering
    \includegraphics[width=\linewidth]{example_extended_timeline}
    \caption{Test plan schedule.}
    \label{fig:test_plan_schedule}
\end{figure}

After generating a successful schedule, the scheduler window replaces the builder scene in the main window automatically.
In the scheduler window, the schedule is described, both quantitatively and graphically.
In this context, the user gets a full understanding of the generated schedule.
The schedule time-line is visualized, in the schedule scene, in an intuitive representation.
Within the schedule scene, the user can navigate, and zoom in/out through the time-line.
Also, the user can distinguish the type of tasks, from their respective drawing on the time-line, as stated in \cref{sssec:schedule_timeline}.
Moreover, the scheduler window can be made the only window of the \gls{TDCSP}, similarly to the builder window.
As a consequence, the approach for representing the schedule time-line resulted in a very smooth user experience.

The main complication of the research question relative to this section, is how to solve an NP-complete problem.
This thesis was able to provide a very convenient model for formulating the project specific problem.
The methodology followed narrows the search space as much as possible, while respecting the precedence relations.
However, for complex test plans, the resulted optimization problem is still time consuming.
Therefore, more improvements have to be done within the solver scope, which is not part of this thesis work.
In this thesis, the solver used is the \gls{CBC} solver, since it is the only free solver available.

\subsection{Error Proof}
The \emph{test plan} design process, proposed by this thesis, is completely error-proof.
The possible errors are avoided, all along the building process, by directing the user during the \emph{test plan} procedure.
The \emph{sanity checks} performed over the \gls{DAG}, designed within the builder, ensure design correctness before generating any schedule.

The implementation of the \gls{TDCSP} provides complete \emph{sanity checks} for: \emph{targets configuration}, \emph{description file}, and \emph{schedule script}.
Therefore, the data files can be re-checked again, and detect any possible malware.
\cref{lst:erroneous_case} shows a simple example of the output message from the \gls{TDCSP}, in the erroneous case.
Notice how the user is directed for easily fixing the error found, within the checked file.
\begin{lstlisting}[numbersep=0pt, basicstyle=\ttfamily, frame=single, breaklines=true, caption={Erroneous case example.}, label={lst:erroneous_case}]
14:45:25,163 - WARNING : Target config C:\_work\Coding\TDCS\data\targets\uMoPS_v5.json doesn't meet schema requirements!

'intt' is not one of ['int', 'float', 'bool'].
    
On instance['tasks'][0]['in'][0]['type']:
"intt"
    
14:45:25,165 - ERROR : Could NOT fetch target's config file C:\_work\Coding\TDCS\data\targets\uMoPS_v5.json!
\end{lstlisting}

\section{Improvements}\label{sec:improvements}
During developing, the implementation of \gls{TDCSP} got very intuitive after several reviews and productive usages from a small user group.
The improvements done can be summarized as follows:
\begin{itemize}
    \item The objective function, of the \emph{optimization problem}, is defined as the accrual of the \glspl{TUF}, of each task. The \glspl{TUF} of the \emph{producer tasks} is multiplied by a weight factor, for emphasizing the importance of the \emph{producer tasks}. Therefore, the latency of any \emph{producer task} is more significant with respect to the objective function. This helps, in getting the optimal scheduler faster.
    \item The automatic \emph{CAN ID} assignment for a target instance, eases the creation of a target instance, and directs the user towards occupying the smallest integers first.
    \item Zooming feature was added for both of the views of the \gls{TDCSP}, in a sense that the user can visualize the percentage zoom ratio.
    \item Keyboard shortcuts were added to almost all actions of the \gls{TDCSP}, also, the key arrows can be used for navigating through the builder view.
    \item Resolution feature was added for the schedule time-line, to better visualize the tricky time-lines (too small/long). 
\end{itemize}