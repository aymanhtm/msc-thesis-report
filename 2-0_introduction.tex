% ~3 pages

\chapter{Introduction}\label{ch:introduction}
Today's market challenges the semiconductors production in several demands, like decreasing time-to-market, increasing manufacturing robustness and quality requirements \cite{Vock2012a}.
On the other hand, the design complexity of the produced \glspl{SoC} has increased.
Therefore assuring dependable systems, requires improvements in the test paradigms already available.
One of those paradigms, is the so-known as \emph{Burn-in} or \emph{Stress Test}.
The latter is the process by which the \gls{DUT} is exercised before launched into market.
Thus trying to force certain failures to occur, under supervised stress conditions.
Burn-in aims at accelerating detection of so-called \emph{infant mortalities} \cite{Ng2008a}, as well as formulating statistical performance sheets.
Such a system, called \gls{MoPS} test system, is being developed at \gls{KAI}.
The \gls{MoPS} architecture works completely in a modularized approach from both \gls{SW} and
\gls{HW} point of views \cite{Steinwender2016}. 
\cref{fig:mops_architecture} shows the architecture of the \gls{MoPS} system.
A basic review, of the architecture of the \gls{MoPS} system, is elaborated in \cref{sec:mops}.
\begin{figure}[htb]
    \centering
    \includegraphics[width=0.6\linewidth]{mops_architecture}
    \caption{\gls{MoPS} architecture.}
    \label{fig:mops_architecture}
\end{figure}

The \gls{MoPS} holds a distributed communication network, which makes it superior in terms of flexibility to a centralized system \cite{Steinwender2016}.
For further extending this flexibility, real-time behavior has to be an option for building the test plan of the \gls{MoPS} system.
There are two major design paradigms for implementing real-time behaviors, the event-triggered and the time-triggered approach \cite{Elmenreich2003a}.
In a time-triggered distributed system, communication takes place according to a common periodic communication schedule.
This thesis is concerned in designing a project specific tool, for generating the schedule of a distributed real-time communication.
The name given for the tool is \gls{TDCSP}.
Therefore, \gls{TDCSP} refers to the developed tool, within the course of this thesis. 

\section{Motivation}\label{sec:motivation}
The non real-time behavior of a test procedure, in the \gls{MoPS} system, resembles the \gls{FSM} model \cite{Steinwender2016}.
The design of the test plan is done by the \gls{TPB}, already developed \cite{Plankensteiner2015}, which aims to ease drawing of the interactive \gls{FSM} diagrams for every layer of the \gls{MoPS} architecture.
Until this point, the test procedure happening on multiple targets has a non real-time behavior, and is composed of event-triggered tasks.
On the other hand a real-time extension of the \gls{MoPS} system, is being researched within a PhD thesis topic.
The PhD thesis focuses on a mixed-triggered communication approach, which provides real-time behavior under normal conditions while it flexibly transitions to a fallback mode when temporal boundaries are not met \cite{Einspieler2021a}.
Therefore, there is a need for a \gls{SW} containing a test plan builder for real-time tasks, and also a scheduler for managing the distributed real-time communication.

Scheduling, in a distributed real-time system, is affected significantly by inter-task communication and hence, a pre-runtime task allocation algorithm is needed \cite{Dar-TzenPeng1997a}, which takes into consideration the real-time constraints.
Moreover, the generated test plan should be as error resistant as possible.
This results in the implementation of several sanity checks during the whole building procedure, and even over the generated schedule script which describes the test plan.

\section{Goals}\label{sec:goals}
The objective of this thesis can be summarized into two key goals.
First, to simplify the creation of a test plan occurring between distributed targets, and generate the optimal schedule time-line.
Second, to verify the sanity requirements of such a designed test plan.
Thus, an intuitive application along with its \gls{GUI} has to be provided.
The latter should be accountable of the following:
\begin{itemize}
    \item Fetching the data files of the available type of targets, for the \gls{MoPS} system, and represent them in a user friendly manner.
    \item Providing a design interface for the inter-task communications, in the form of simple data dependency graphs.
    \item Verifying the correctness of the built design, and formulating it into a well structured model.
    \item Generating the optimal schedule, while considering all the transmission delays which are network dependable.
    \item Visualizing the generated schedule time-line in a smooth representation.
    \item Outputting both the test plan description and the schedule script in \gls{JSON} format, respecting their defined schemas. Also, providing complete correctness check for the last-mentioned files.
\end{itemize}
The application is completely developed in \emph{Python 3} language.
Thus, it is benefitted from all the relevant available Python modules.
For the \gls{GUI} representations, \emph{PyQt5} libraries and modules, are used for developing the tool.
\cref{sec:programming_languages} presents a review of the available programming languages and alternatives that could have been chosen.

\section{Problem Statement}\label{sec:problem_statement}
This thesis emerges from the need of a real-time communication scheduling tool, for the time-triggered extension of the \gls{MoPS} test plan.
Therefore, the problem statement is divided into three areas:
\begin{itemize}
    \item How to represent the creation process of the test plan, to provide an intuitive user friendly interface?
        \begin{itemize}
            \item How to visualize the type of targets, with all its description?
            \item How to construct the dependency graph of the inter-task communications?
            \item What schema to follow for better non-redundant description of the test plan file?
        \end{itemize}
    \item What scheduling algorithm should be used, for fulfilling the project specific requirements?
        \begin{itemize}
            \item How to integrate the communication delays in the scheduling problem?
            \item How to benefit of maximum \gls{CPU} utility?
            \item How to visualize the resulted schedule time-line, in an intelligible way?
            \item What schema to follow for better non-redundant description of the schedule script file? 
        \end{itemize}
    \item How to make sure that the test plan is as error proof as possible?
        \begin{itemize}
            \item How to prevent errors during the building process of the test plan?
            \item How to re-check for errors in a test plan description?
            \item How to re-check for errors in a schedule script file?
        \end{itemize}
\end{itemize}

\section{Thesis Outline}\label{sec:thesis_outline}
This section gives some reading advice, for easing the understanding of this thesis work.
The introduction is exhibited in \cref{ch:introduction}, where the motivation, goals, and problems statement are elaborated. It is highly recommended for getting a basic overview of the direction of this thesis.
A literature review is presented in \cref{ch:literature_review}, for emphasizing on some concepts that are used during the course of this thesis. The latter explains:
\begin{itemize}
    \item The different scheduling paradigm concepts, which are necessary for understanding the need of implementing a project specific scheduling algorithm. 
    \item The overview of the parent project \gls{MoPS}, from which this thesis emerges. This part is a must for getting a complete picture of the system model, later explained in \cref{ch:system_model}.
    \item \gls{SW} and mathematical basic concepts, used within the modeling of the problem statement researched by this thesis. These concepts can be skipped, if there is already a relative background.
\end{itemize} 
\cref{ch:system_model} explains the system model, and the paradigm followed behind the intuitive scheduling algorithm.
\cref{ch:implementation} exhibits the full creation of the application, from both core and \gls{GUI} aspects.
Finally, an evaluation of the implementation is done in \cref{ch:evaluations} and the conclusion with the outlook, follows in \cref{ch:conclusion_and_outlook}.