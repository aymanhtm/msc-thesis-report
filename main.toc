\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Motivation}{2}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Goals}{2}{section.1.2}% 
\contentsline {section}{\numberline {1.3}Problem Statement}{3}{section.1.3}% 
\contentsline {section}{\numberline {1.4}Thesis Outline}{4}{section.1.4}% 
\contentsline {chapter}{\numberline {2}Literature Review}{5}{chapter.2}% 
\contentsline {section}{\numberline {2.1}JavaScript Object Notation}{5}{section.2.1}% 
\contentsline {section}{\numberline {2.2}CAN Bus}{5}{section.2.2}% 
\contentsline {section}{\numberline {2.3}MoPS}{7}{section.2.3}% 
\contentsline {subsection}{\numberline {2.3.1}Targets Configuration}{8}{subsection.2.3.1}% 
\contentsline {section}{\numberline {2.4}Scheduling Paradigms}{8}{section.2.4}% 
\contentsline {subsection}{\numberline {2.4.1}Scheduling Problem}{10}{subsection.2.4.1}% 
\contentsline {subsection}{\numberline {2.4.2}Scheduling Algorithms}{11}{subsection.2.4.2}% 
\contentsline {subsubsection}{\nonumberline Scheduling Time-Triggered Communication}{12}{section*.6}% 
\contentsline {section}{\numberline {2.5}Computational Complexity Theory}{13}{section.2.5}% 
\contentsline {section}{\numberline {2.6}Time Utility Function}{14}{section.2.6}% 
\contentsline {section}{\numberline {2.7}Programming Languages}{14}{section.2.7}% 
\contentsline {subsection}{\numberline {2.7.1}Graphical User Interfaces}{15}{subsection.2.7.1}% 
\contentsline {chapter}{\numberline {3}System Model}{17}{chapter.3}% 
\contentsline {section}{\numberline {3.1}General Process}{17}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Task Model}{19}{section.3.2}% 
\contentsline {subsection}{\numberline {3.2.1}Raw Data Representation}{19}{subsection.3.2.1}% 
\contentsline {subsection}{\numberline {3.2.2}Task Types}{20}{subsection.3.2.2}% 
\contentsline {section}{\numberline {3.3}System Complication}{21}{section.3.3}% 
\contentsline {section}{\numberline {3.4}Data Analysis}{25}{section.3.4}% 
\contentsline {subsection}{\numberline {3.4.1}Internal Dependencies}{25}{subsection.3.4.1}% 
\contentsline {subsection}{\numberline {3.4.2}Network Dependencies}{25}{subsection.3.4.2}% 
\contentsline {subsubsection}{\nonumberline Consumer Dependencies}{26}{section*.7}% 
\contentsline {subsubsection}{\nonumberline Producer Dependencies}{27}{section*.8}% 
\contentsline {subsubsection}{\nonumberline Transmission Time Computation}{28}{section*.9}% 
\contentsline {section}{\numberline {3.5}Time-Line Model}{30}{section.3.5}% 
\contentsline {subsection}{\numberline {3.5.1}Time Slots}{31}{subsection.3.5.1}% 
\contentsline {subsection}{\numberline {3.5.2}Task Delays}{31}{subsection.3.5.2}% 
\contentsline {section}{\numberline {3.6}Task Set Transformation}{32}{section.3.6}% 
\contentsline {section}{\numberline {3.7}Optimization Problem}{32}{section.3.7}% 
\contentsline {subsection}{\numberline {3.7.1}Period Bound}{33}{subsection.3.7.1}% 
\contentsline {subsection}{\numberline {3.7.2}Objective Function}{34}{subsection.3.7.2}% 
\contentsline {subsection}{\numberline {3.7.3}Inequality Constraints}{34}{subsection.3.7.3}% 
\contentsline {chapter}{\numberline {4}Implementation}{37}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Software Architecture}{37}{section.4.1}% 
\contentsline {section}{\numberline {4.2}User Interface}{39}{section.4.2}% 
\contentsline {subsection}{\numberline {4.2.1}Targets Tree}{39}{subsection.4.2.1}% 
\contentsline {subsubsection}{\nonumberline New Target Dialog}{41}{section*.10}% 
\contentsline {subsubsection}{\nonumberline Modify Target Dialog}{42}{section*.11}% 
\contentsline {subsection}{\numberline {4.2.2}Builder View}{43}{subsection.4.2.2}% 
\contentsline {subsubsection}{\nonumberline Task Item}{43}{section*.12}% 
\contentsline {subsubsection}{\nonumberline Modify Parameters Dialog}{44}{section*.13}% 
\contentsline {subsubsection}{\nonumberline Connection Item}{45}{section*.14}% 
\contentsline {subsection}{\numberline {4.2.3}Scheduler View}{45}{subsection.4.2.3}% 
\contentsline {subsubsection}{\nonumberline Schedule Table}{46}{section*.15}% 
\contentsline {subsubsection}{\nonumberline Schedule Time-line}{47}{section*.16}% 
\contentsline {subsection}{\numberline {4.2.4}Prompt View}{48}{subsection.4.2.4}% 
\contentsline {subsection}{\numberline {4.2.5}Main Window}{48}{subsection.4.2.5}% 
\contentsline {subsubsection}{\nonumberline Preference Settings Dialog}{49}{section*.17}% 
\contentsline {subsubsection}{\nonumberline Display Settings Dialog}{50}{section*.18}% 
\contentsline {subsubsection}{\nonumberline Help Browser}{51}{section*.19}% 
\contentsline {section}{\numberline {4.3}Core Modules}{51}{section.4.3}% 
\contentsline {subsection}{\numberline {4.3.1}Targets Core}{52}{subsection.4.3.1}% 
\contentsline {subsection}{\numberline {4.3.2}Builder Core}{54}{subsection.4.3.2}% 
\contentsline {subsection}{\numberline {4.3.3}Scheduler Core}{55}{subsection.4.3.3}% 
\contentsline {subsection}{\numberline {4.3.4}Sanity Checks}{56}{subsection.4.3.4}% 
\contentsline {subsection}{\numberline {4.3.5}Main Actions}{57}{subsection.4.3.5}% 
\contentsline {subsubsection}{\nonumberline File Menu}{57}{section*.20}% 
\contentsline {subsubsection}{\nonumberline Edit Menu}{59}{section*.21}% 
\contentsline {subsubsection}{\nonumberline View Menu}{59}{section*.22}% 
\contentsline {subsubsection}{\nonumberline Insert Menu}{59}{section*.23}% 
\contentsline {subsubsection}{\nonumberline Help Menu}{59}{section*.24}% 
\contentsline {section}{\numberline {4.4}Data Base}{60}{section.4.4}% 
\contentsline {subsection}{\numberline {4.4.1}Configuration Files}{60}{subsection.4.4.1}% 
\contentsline {subsection}{\numberline {4.4.2}Target Description}{60}{subsection.4.4.2}% 
\contentsline {subsection}{\numberline {4.4.3}Project Description File}{61}{subsection.4.4.3}% 
\contentsline {subsection}{\numberline {4.4.4}Logging}{61}{subsection.4.4.4}% 
\contentsline {subsection}{\numberline {4.4.5}Schedule Description}{62}{subsection.4.4.5}% 
\contentsline {subsection}{\numberline {4.4.6}Schedule Script}{62}{subsection.4.4.6}% 
\contentsline {chapter}{\numberline {5}Evaluations}{63}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Results}{63}{section.5.1}% 
\contentsline {subsection}{\numberline {5.1.1}Test Plan Builder}{63}{subsection.5.1.1}% 
\contentsline {subsection}{\numberline {5.1.2}Test Plan Schedule}{64}{subsection.5.1.2}% 
\contentsline {subsection}{\numberline {5.1.3}Error Proof}{66}{subsection.5.1.3}% 
\contentsline {section}{\numberline {5.2}Improvements}{66}{section.5.2}% 
\contentsline {chapter}{\numberline {6}Conclusion \& Outlook}{69}{chapter.6}% 
\contentsline {section}{\numberline {6.1}Thesis Contributions}{69}{section.6.1}% 
\contentsline {section}{\numberline {6.2}Future Works}{70}{section.6.2}% 
\contentsline {chapter}{\nonumberline Bibliography}{73}{chapter*.27}% 
